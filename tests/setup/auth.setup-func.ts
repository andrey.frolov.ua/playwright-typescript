import { expect } from '@playwright/test';
import { test as setup } from 'framework/fixtures/ObjectFixture';
import { NOT_SIGNED_IN_STATE, SIGNED_IN_STATE } from 'framework/utils/Constants';

setup.describe('Executing sign-in pre-condition for tests', () => {
  setup('Authenticate with a known user', async ({ commonSteps }) => {
    await commonSteps.login();
  });
});
