import { Page } from "@playwright/test";
import { CredentialDto, EnvCredentialsDto } from "framework/dto/Credentials.dto";
import { AmemberLoginPage } from "framework/pages/signin/AmemberLoginPage";
import { LocalLoginPage } from "framework/pages/signin/LocalLoginPage";
import _credentials from "framework/resources/.auth/credentials.json";
export class CommonSteps {

    private page: Page;

    constructor(page: Page) {
        this.page = page;
    }

    async login(): Promise<void> {
        const credentials = _credentials as unknown as EnvCredentialsDto;
        const url = process.env.BASE_SITE_URL; //Note: URL is not used
        const environment = process.env.ENV?.toLocaleUpperCase();
        console.log(`Current env is ${environment}`);
        console.log(`Current env url is ${url}`);
    
        if (environment === "MSC2-PROD") {
            await this.velocityLocalLogin(credentials.MSC2);
        } else if (environment === "UAT") {
            await this.velocityAmemberLogin(credentials.UAT_AMEMBER);
        } else if (environment === "SIT_AMEMBER") {
            await this.velocityLocalLogin(credentials.UAT_AMEMBER);
            await this.page.goto(process.env.LOGIN_URL!);
            await this.openVelocity();
        } else if (environment === "DEVJASON") {
            await this.velocityLocalLogin(credentials.DEV_JASON);
        } else if (environment === "NEW_MEXICO_AMEMBER") {
            await this.velocityAmemberLogin(credentials.NEW_MEXICO_AMEMBER);
        } else  {
            await this.velocityLocalLogin(credentials.LOCAL);
        }
       //await this.page.reload();
        // ?
    }


    private async velocityLocalLogin(credentials: CredentialDto): Promise<void> {
        await this.navigateToEnv();
        const loginPage = new LocalLoginPage(this.page)
        await loginPage.open();
        await loginPage.login(credentials.USER_NAME, credentials.PASSWORD);
    }

    private async velocityAmemberLogin(credentials: CredentialDto): Promise<void> {
        await this.navigateToEnv();
        const loginPage = new AmemberLoginPage(this.page)
        await loginPage.open();
        await loginPage.login(credentials.USER_NAME, credentials.PASSWORD);

        await this.openVelocity();

    }

    async navigateToEnv() {
        //Not sure what should be here, you did not mention
    }

    async openVelocity() {
        //Not sure what should be here, you did not mention
    }
}