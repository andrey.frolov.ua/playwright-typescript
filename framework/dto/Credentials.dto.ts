export type CredentialDto = {
  USER_NAME: string;
  ACCOUNT_NAME: string;
  PASSWORD: string;
};

export type EnvCredentialsDto = {
  MSC2: CredentialDto,
  UAT_AMEMBER: CredentialDto,
  NEW_MEXICO_AMEMBER: CredentialDto,
  LOCAL: CredentialDto,
  DEV_JASON: CredentialDto
}
