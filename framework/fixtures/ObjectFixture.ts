import { test as base } from "@playwright/test";
import { LocalLoginPage } from "framework/pages/signin/LocalLoginPage";
import { ProfilePage } from "framework/pages/profile/ProfilePage";
import { BookStorePage } from "framework/pages/bookstore/BookStorePage";
import { BookDetailsPage } from "framework/pages/bookstore/BookDetailsPage";
import { TestDataProvider } from "framework/utils/TestDataProvider";
import { AmemberLoginPage } from "framework/pages/signin/AmemberLoginPage";
import { CommonSteps } from "framework/steps/CommonSteps";


type Objects = {
  amemberLoginPage: AmemberLoginPage;
  localLoginPage: LocalLoginPage;
  profilePage: ProfilePage;
  bookStorePage: BookStorePage;
  bookDetailsPage: BookDetailsPage;
  testDataProvider: TestDataProvider;
  commonSteps: CommonSteps;
};

const testExtendedWithPages = base.extend<Objects>({
  localLoginPage: async ({ page }, use) => {
    await use(new LocalLoginPage(page));
  },
  amemberLoginPage: async ({ page }, use) => {
    await use(new AmemberLoginPage(page));
  },
  profilePage: async ({ page }, use) => {
    await use(new ProfilePage(page));
  },
  bookStorePage: async ({ page }, use) => {
    await use(new BookStorePage(page));
  },
  bookDetailsPage: async ({ page }, use) => {
    await use(new BookDetailsPage(page));
  },
  testDataProvider: async ({}, use) => {
    await use(new TestDataProvider());
  },
  commonSteps: async ({ page }, use) => {
    await use(new CommonSteps(page));
  },
});
export const test = testExtendedWithPages;
export const expect = test.expect;
export const slowExpect = expect.configure({ timeout: 10000 });
export const extrimelySlowExpect = expect.configure({ timeout: 20000 });
