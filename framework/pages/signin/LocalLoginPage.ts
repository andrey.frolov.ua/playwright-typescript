import { Locator, Page } from "@playwright/test";
import { BasePage } from "framework/pages/BasePage";

export class LocalLoginPage extends BasePage {
  //locators
  readonly userNameInput: Locator = this.page.locator("//input[@name='Username']");
  readonly passwordInput: Locator = this.page.locator("//input[@name='Password']");
  readonly submitBtn: Locator = this.page.locator("button");


  //methods
  public async login(username: string, password: string): Promise<void> {
    console.log(`Attemting to login to Local login page using credentials: ${username}, ${password}`);
    //uncomment for real env

    // await this.userNameInput.fill(username);
    // await this.passwordInput.fill(password);
    // await this.submitBtn.click();
  }

  pageUrl(): string {
    return "/login";
  }
}
